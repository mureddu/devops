import requests
import pytest
import logging


@pytest.mark.parametrize("port, path, expected_status, excepted_out",
[
    (7070, "gitlab-version", 200, "Version:"),
    (7070, "gitlab-version2", 404, "Not my job"),
    (7071, "gitlab-version", 0, "Couldn't connect")
])
def test_app_answers(port, path, expected_status, excepted_out):
    """
    Send a request to our app then make sure it answered corrrectly.
    """
    logging.info(f"Trying to get an answer on port {port} with path '{path}'")
    status, output = send_request(port, path)
    assert status == expected_status
    assert excepted_out in output


def send_request(port, path):
    """
    Send a customized request to our app.
    """
    try:
        req = requests.get(f"http://localhost:{port}/{path}", timeout=3)
        return req.status_code, req.text
    except:
        logging.info("Couldn't connect to the requested port")
        return 0, "Couldn't connect"
