FROM debian:bullseye

COPY requirements.txt .
COPY *.py ./
RUN apt update && apt install python3-pip -y 
RUN pip3 install -r requirements.txt
