import os
import requests
import json
from http.server import BaseHTTPRequestHandler, HTTPServer


class myHandler(BaseHTTPRequestHandler):
    """
    Handles basic HTTP requests.
    """

    def do_GET(self):
        """
        Answers GET requests, a part of it at least.
        """
        path = self.path
        print(f"requested path: {path}")
        if self.path == "/gitlab-version":
            print("This I can handle")
            result, output = retrieve_gitlab_version()
            print(output)
            if result:
                self.return_all_good(output)
            else:
                self.return_nok(output)
            return
        self.return_nok("Not my job")
        return

    def return_all_good(self, gitlab_ver):
        """
        Answers positively to GET requests.
        """
        self.send_response(200)
        self.send_header("Access-Control-Allow-Origin","*")
        self.end_headers()
        self.wfile.write(gitlab_ver.encode("utf-8"))  
    
    def return_nok(self, error_message):
        """
        Answers negatively to GET requests.
        """
        print(error_message)
        self.send_response(404)
        self.send_header("Access-Control-Allow-Origin","*")
        self.end_headers()
        self.wfile.write(error_message.encode("utf-8"))


def retrieve_gitlab_version():
    """
    Requests gitlab's current version, if it goes fine returns it else returns an error message.
    """
    try: 
        api_token = os.environ["PRIVATE_TOKEN"]
        out = requests.get(
            "https://gitlab.com/api/v4/version",
            headers={"PRIVATE-TOKEN": api_token},
            timeout=3)
        if out.status_code == 200:
            answer = json.loads(out.text)
            return True, f"Version:{answer['version']};{answer['revision']}"
        else:
            error_message = f"Couldn't retrieve gitlab version, got response {out.status_code}"
            return False, error_message
    except:
        error_message = f"Couldn't retrieve gitlab version, something went wrong"
        return False, error_message


def start_server():
    """
    Starts a http server on port 7070.
    """
    try:
        print("Starting http server :")
        server_address = ('', 7070)
        server = HTTPServer(server_address, myHandler)
        server.serve_forever()
    except KeyboardInterrupt:
        print('^C received, shutting down the web server')
    except Exception as e:
        print(e)
        raise IOError("did not start server %(exception)s" % {'exception': e})

if __name__ == "__main__":

    start_server()
